<?php
/*
Plugin Name: Estúdio Cimbre
Plugin URI: https://estudiocimbre.com.br
Description: Custom post types and Custom post fields for Estúdio Cimbre site
Author: Estúdio Cimbre
Version: 1.0.0.1
Author URI: https://www.estudiocimbre.com.br/
Text Domain: estudiocimbre
*/

/**
 * Load Translation
 */
function estudiocimbre_load_textdomain() 
{
    load_muplugin_textdomain('estudiocimbre', basename(dirname(__FILE__)) . '/languages');
}
add_action('plugins_loaded', 'estudiocimbre_load_textdomain');

/**
 *
 * Register post types
 *
 */

/**
 * Works
 */
function cpt_estudiocimbre_works() 
{
    $labels = array(
            'name'                  => __('Works', 'estudiocimbre'),
            'singular_name'         => __('Work', 'estudiocimbre'),
            'menu_name'             => __('Works', 'estudiocimbre'),
            'name_admin_bar'        => __('Work', 'estudiocimbre'),
            'archives'              => __('Work Archives', 'estudiocimbre'),
            'attributes'            => __('Work Attributes', 'estudiocimbre'),
            'parent_item_colon'     => __('Parent Work:', 'estudiocimbre'),
            'all_items'             => __('All Works', 'estudiocimbre'),
            'add_new_item'          => __('Add New Work', 'estudiocimbre'),
            'add_new'               => __('Add work', 'estudiocimbre'),
            'new_item'              => __('New Work', 'estudiocimbre'),
            'edit_item'             => __('Edit Work', 'estudiocimbre'),
            'update_item'           => __('Update Work', 'estudiocimbre'),
            'view_item'             => __('View Work', 'estudiocimbre'),
            'view_items'            => __('View Works', 'estudiocimbre'),
            'search_items'          => __('Search Work', 'estudiocimbre'),
            'not_found'             => __('Not found', 'estudiocimbre'),
            'not_found_in_trash'    => __('Not found in Trash', 'estudiocimbre'),
            'featured_image'        => __('Featured Image', 'estudiocimbre'),
            'set_featured_image'    => __('Set featured image', 'estudiocimbre'),
            'remove_featured_image' => __('Remove featured image', 'estudiocimbre'),
            'use_featured_image'    => __('Use as featured image', 'estudiocimbre'),
            'insert_into_item'      => __('Insert into work', 'estudiocimbre'),
            'uploaded_to_this_item' => __('Uploaded to this work', 'estudiocimbre'),
            'items_list'            => __('Works list', 'estudiocimbre'),
            'items_list_navigation' => __('Works list navigation', 'estudiocimbre'),
            'filter_items_list'     => __('Filter Works list', 'estudiocimbre'),
    );
    $rewrite = array(
            'slug'                  => __('works', 'estudiocimbre'),
            'with_front'            => false,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Work', 'estudiocimbre'),
            'description'           => __('Estúdio Cimbre Works', 'estudiocimbre'),
            'labels'                => $labels,
            'supports'              => array('title','editor', 'thumbnail', 'revisions', 'page-attributes'),
            'taxonomies'            => array('category', 'post_tag'),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 7,
            'menu_icon'             => 'dashicons-admin-page',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('cimbre_works', $args); 
} 
add_action('init', 'cpt_estudiocimbre_works', 0);

/**
 * Works Custom Fields
 */
function cmb_estudiocimbre_works() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_cimbre_works_';
    
    /**
    * Initiate the metabox
    */
    $cmb_works = new_cmb2_box(
        array(
            'id'            => 'estudiocimbre_works',
            'title'         => __('Work', 'estudiocimbre'),
            'object_types'  => array('cimbre_works'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    // Local
    $cmb_works->add_field( 
        array(
            'name'       => __('Client', 'estudiocimbre'),
            'desc'       => '',
            'id'         => $prefix . 'client',
            'type'       => 'text',
            //'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        )
    );

    //Date
    $cmb_works->add_field( 
        array(
            'name'       => __('Date', 'estudiocimbre'),
            'desc'       => '',
            'id'         => $prefix . 'date', //Date 
            'type' => 'text',
            'attributes' => array(
                'type' => 'number',
                'pattern' => '\d*',
            ),
        ) 
    );

    //Gallery Title
    $cmb_works->add_field( 
        array(
        'name' => __('Gallery', 'estudiocimbre'),
        'desc' => '',
        'type' => 'title',
        'id'   => $prefix.'gallery_title',
        )
    );

    //Gallery Description
    $cmb_works->add_field( 
        array(
        'name' => 'Description',
        'desc' => '',
        'id' => $prefix.'gallery_description',
        'type' => 'textarea'
        ) 
    );

    //Gallery Images
    $cmb_works->add_field(
        array(
            'name' => __('Images', 'estudiocimbre'),
            'id'   => $prefix.'gallery_images',
            'type' => 'file_list',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text' => array(
                'add_upload_files_text' => __('Add Images', 'estudiocimbre'), // default: "Add or Upload Files"
                'remove_image_text' => __('Remove Image', 'estudiocimbre'), // default: "Remove Image"
                'file_text' => __('File', 'estudiocimbre'), // default: "File:"
                'file_download_text' => __('Download', 'estudiocimbre'), // default: "Download"
                'remove_text' => __('Remove', 'estudiocimbre'),
                'use_text' => __('Remove', 'estudiocimbre'), // default: "Remove"
                'upload_file'  => __('Use this file', 'estudiocimbre'),
                'upload_files' => __('Use these files', 'estudiocimbre'),
                'remove_image' => __('Remove Image', 'estudiocimbre'),
                'remove_file'  => __('Remove', 'estudiocimbre'),
                'file'         => __('File:', 'estudiocimbre'),
                'download'     => __('Download', 'estudiocimbre'),
                'check_toggle' => __('Select / Deselect All', 'estudiocimbre'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(160, 90) //'large', // Image size to use when previewing in the admin.
        )
    );
}
add_action('cmb2_admin_init', 'cmb_estudiocimbre_works');

?>
